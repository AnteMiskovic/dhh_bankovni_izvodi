<?php

require_once 'functions.php';

/* BANKOVNI IZVODI - Admin sučelje
* @param array $vars
*/
function adminAreaBankovniIzvodi($vars)
{
  if ($vars['pagetitle'] == 'DHH Bankovni izvodi') {
    $opcijeModula = opcijedhhbiModula();
    $html = <<<HTML
    <link rel="stylesheet" href=$opcijeModula[sistemrootURL]/modules/addons/dhh_bankovni_izvodi/inc/font-awesome/css/font-awesome.min.css>
    <link rel="stylesheet" type="text/css" media="all" href=$opcijeModula[sistemrootURL]/modules/addons/dhh_bankovni_izvodi/inc/style.css>
HTML;
    return $html;
  }
}

/* IZMJENA PONUDE - Admin sučelje (napomena)
* @param array $vars
*/
function adminAreaInvoiceEdit($vars)
{
  $notes = '';
  $db = new DHHbankovniizvodiDB();

  if ($_POST['aktivnost'] == 'internal_notes_edit') {
    if(!empty($vars['invoiceid'])) {
      $notes = $_POST['internal_notes'];
      $db->where('id', $vars['invoiceid'])->update('tblinvoices', array('notes_internal' => $_POST['internal_notes']));
    }
  }
  else {
    if(!empty($vars['invoiceid'])) {
      $notes = $db->where('id', $vars['invoiceid'])->getValue('tblinvoices', 'notes_internal');
    }
  }

  $html = '
  <br />
  <form method="post" id="internal_notes_form" name="internal_notes_form">
    <input type="hidden" name="aktivnost" value="internal_notes_edit" />
    INTERNA NAPOMENA (NIJE VIDLJIVO NA PONUDI)
    <textarea rows="4" style="width:100%" name="internal_notes" class="form-control">'.$notes.'</textarea>
    <div class="btn-container">
      <input type="submit" value="Save notes" class="btn btn-primary">
    </div>
  <div style="clear:both"></div>
  </form>
  ';

  return $html;
}

add_hook("AdminAreaHeadOutput", 1, "adminAreaBankovniIzvodi");

add_hook('AdminInvoicesControlsOutput', 1, "adminAreaInvoiceEdit");
