<?php


require_once('db.php');

if (!defined('ROOTDIR')) {
  define('ROOTDIR', realpath(dirname(__FILE__) . "/../../../"));
}

/**
 * Funkcija za uzimanje svih bitnih opcija
 * @return array
 */
if (!function_exists('opcijedhhbiModula')) {
  function opcijedhhbiModula()
  {
    global $CONFIG, $customadminpath, $adminpath, $LANG;
    $_OPCIJE = array();
    $db = new DHHbankovniizvodiDB();

    # lokacije foldera
    $_OPCIJE['adminpath']       = ($customadminpath ? $customadminpath : "");
    # WHMCS postavke
    $_OPCIJE['sistemrootURL']   = ($CONFIG['SystemSSLURL'] != '' ? rtrim($CONFIG['SystemSSLURL'], '/') : rtrim($CONFIG['SystemURL'], '/'));
    $_OPCIJE['sistemURL']       = ($CONFIG['SystemSSLURL'] != '' ? rtrim($CONFIG['SystemSSLURL'] . '/' . $_OPCIJE['adminpath'], '/') : rtrim($CONFIG['SystemURL'] . '/' . $_OPCIJE['adminpath'], '/'));
    $_OPCIJE['modulelink']      = $_OPCIJE['sistemURL'] . '/dhh_bankovni_izvodi/index.php?module=dhh_bankovni_izvodi';

    # postavke modula
    $_OPCIJE['modulverzija']    = $db->where('module', 'dhh_bankovni_izvodi')->where('setting', 'version')->getValue('tbladdonmodules', 'value');
    $_OPCIJE['modulefolder']    = ROOTDIR . '/modules/addons/dhh_bankovni_izvodi';
    $_OPCIJE['izdavatelj_izvoda']    = $db->where('opcija', 'izdavatelj_izvoda')->getValue('mod_dhhbankovniizvodi', 'vrijednost');
    $_OPCIJE['lokacija_izvoda']      = $db->where('opcija', 'lokacija_izvoda')->getValue('mod_dhhbankovniizvodi', 'vrijednost');

    return $_OPCIJE;
  }
}

if (!function_exists('fileUpload')) {
  function fileUpload($file)
  {
    if ($file["name"] == '') {
      return returnMsg(false, "Datoteka nije odabrana.");
    }

    $options = opcijedhhbiModula();
    $db = new DHHbankovniizvodiDB();

    $izdavatelj_izvoda = $options['izdavatelj_izvoda'];
    $target_file = $options['lokacija_izvoda'] . '/' . basename($file["name"]);
    $uploadOk = 1;
    $fileType = strtolower(pathinfo($target_file, PATHINFO_EXTENSION));


    $check = filesize($file["tmp_name"]);
    if ($check !== false) {
      $uploadOk = 1;
    } else {
      $uploadOk = 0;
      return returnMsg($uploadOk, "Datoteka nije XML.");
    }


    // Check file size
    if ($file["size"] > 500000) {
      $uploadOk = 0;
      return returnMsg($uploadOk, "Datoteka je prevelika");
    }

    // Check if file already exists
    if (file_exists($target_file)) {
      $uploadOk = 0;
      return returnMsg($uploadOk, "Datoteka s tim nazivom već postoji na serveru");
    }

    // Allow certain file formats
    if ($fileType != "xml") {
      $uploadOk = 0;
      return returnMsg($uploadOk, "Samo XML datoteke su dozvoljene");
    }

    $xml = simplexml_load_file($file["tmp_name"]);

    $fromDate = $xml->BkToCstmrStmt->Stmt->FrToDt->FrDtTm->__toString();
    $toDate = $xml->BkToCstmrStmt->Stmt->FrToDt->ToDtTm->__toString();


    $fromDate = str_replace("T", " ", $fromDate);
    $toDate = str_replace("T", " ", $toDate);

    if ($izdavatelj_izvoda == 'ZABA') {
      $fromDate = DateTime::createFromFormat('Y-m-d H:i:s.u', $fromDate);
      $fromDate->setTime(0, 0, 0, 0);
      $fromDate = $fromDate->format('Y-m-d H:i:s');

      $toDate = DateTime::createFromFormat('Y-m-d H:i:s.u', $toDate);
      $toDate->setTime(23, 59, 59, 0);
      $toDate = $toDate->format('Y-m-d H:i:s');
    }

    $provjeraIzvodaUBazi = "SELECT * FROM `mod_dhhbankovniizvodi_datoteke` WHERE `izvod_od` = '$fromDate' AND `izvod_do` = '$toDate' AND `izdavatelj` = '$izdavatelj_izvoda'";
    $postojeci_izvod = $db->rawQuery($provjeraIzvodaUBazi);
    if (!empty($postojeci_izvod) == 0) {
      $db->insert("mod_dhhbankovniizvodi_datoteke", array(
        "naziv_datoteke" => $file["name"],
        "izvod_od" => $fromDate,
        "izvod_do" => $toDate,
        "izdavatelj" => $izdavatelj_izvoda
      ));
      $insertFileID = $db->getInsertId();
    } else {
      return returnMsg(false, 'Izvod za period od ' . date('d.m.Y H:i:s', strtotime($fromDate)) . ' do ' . date('d.m.Y H:i:s', strtotime($toDate)) . ' već postoji u bazi');
    }

    if ($uploadOk == 0) {
      return returnMsg($uploadOk, "Datoteka nije spremljena na server");
    } else {
      if (move_uploaded_file($file["tmp_name"], $target_file)) {

        $transakcije_sve = $xml->BkToCstmrStmt;

        foreach ($transakcije_sve->Stmt as $transakcije_poValuti) {
          foreach ($transakcije_poValuti->Ntry as $transakcija) {
            $iznos = $transakcija->Amt;
            $valuta = $transakcija->Amt['Ccy'];
            $oznaka_transakcije = $transakcija->AcctSvcrRef;
            $platitelj = $transakcija->NtryDtls->TxDtls->RltdPties->Dbtr->Nm;
            $isplata = $transakcija->NtryDtls->TxDtls->RltdPties->Cdtr->Nm;
            $poziv_na_broj = $transakcija->NtryDtls->TxDtls->RmtInf->Strd->CdtrRefInf->Ref;
            $opis_placanja = $transakcija->NtryDtls->TxDtls->RmtInf->Strd->AddtlRmtInf;
            $opis_placanja_devizni = $transakcija->NtryDtls->TxDtls->RmtInf->Ustrd;

            ($iznos != null) ? $iznos_value = $iznos->__toString() : $iznos_value = null;
            ($valuta != null) ? $valuta_value = $valuta->__toString() : $valuta_value = null;
            ($oznaka_transakcije != null) ? $oznaka_transakcije_value = $oznaka_transakcije->__toString() : $oznaka_transakcije_value = null;
            ($platitelj != null) ? $platitelj_value = $platitelj->__toString() : $platitelj_value = null;
            ($isplata != null) ? $isplata_value = $isplata->__toString() : $isplata_value = null;
            ($poziv_na_broj != null) ? $poziv_na_broj_value = $poziv_na_broj->__toString() : $poziv_na_broj_value = null;
            ($opis_placanja != null) ? $opis_placanja_value = $opis_placanja->__toString() : $opis_placanja_value = null;
            ($opis_placanja_devizni != null) ? $opis_placanja_devizni_value = $opis_placanja_devizni->__toString() : $opis_placanja_devizni_value = null;

            $opis = ($valuta_value == "HRK") ? $opis_placanja_value : $opis_placanja_devizni_value;

            $db->insert("mod_dhhbankovniizvodi_transakcije", array(
              "datoteka_ID" => $insertFileID,
              "oznaka_transakcije" => $oznaka_transakcije_value,
              "platitelj" => $platitelj_value,
              "isplata" => $isplata_value,
              "poziv_na_broj" => substr($poziv_na_broj_value, 4),
              "iznos" => $iznos_value,
              "valuta" => $valuta_value,
              "opis_placanja" => $opis
            ));
          }
        }

        return returnMsg(true, "Datoteka " . htmlspecialchars(basename($file["name"])) . " je spremljena na server.");
      } else {
        return returnMsg(false, "Došlo je do greške prilikom spremanja datoteke");
      }
    }
  }
}

if (!function_exists('scanUploadDir')) {
  function scanUploadDir()
  {
    $options = opcijedhhbiModula();

    $file_list = array_diff(scandir($options['lokacija_izvoda']), array('.', '..'));
    return $file_list;
  }
}

if (!function_exists('returnMsg')) {
  function returnMsg($success, $msg)
  {
    return array($success, $msg);
  }
}

if (!function_exists('readReturnMsg')) {
  function readReturnMsg($array)
  {
    $status = $array[0];
    if ($status) {
      $html = '<p class="success">' . $array[1] . '</p>';
    } else {
      $html = '<p class="error">' . $array[1] . '</p>';
    }

    return $html;
  }
}

if (!function_exists('dohvatiTransakcijeZaDatum')) {
  function dohvatiTransakcijeZaDatum($datum)
  {
    $db = new DHHbankovniizvodiDB();
    $date = DateTime::createFromFormat('d/m/Y', $datum);
    $date->setTime(0, 0, 0);
    $queryDate = $date->format('Y-m-d H:i:s');
    $transakcije = null;

    $fileId = $db->where('izvod_od', $queryDate)->getValue('mod_dhhbankovniizvodi_datoteke', 'id');

    if ($fileId != null) {
      $transakcije = $db->where('datoteka_ID', $fileId['id'])->get("mod_dhhbankovniizvodi_transakcije");
    }

    return $transakcije;
  }
}

if (!function_exists('dohvatiPonudu')) {
  function dohvatiPonudu($invoicenum)
  {
    $db = new DHHbankovniizvodiDB();
    $ponude = null;

    $ponude = $db->where('invoicenum', $invoicenum)->getOne("tblinvoices");

    return $ponude;
  }
}

if (!function_exists('dohvatiKorisnika')) {
  function dohvatiKorisnika($userid)
  {
    $db = new DHHbankovniizvodiDB();
    $korisnik = null;

    $korisnik = $db->where('id', $userid)->getOne("tblusers");

    return $korisnik;
  }
}

if (!function_exists('dohvatiKlijenta')) {
  function dohvatiKlijenta($userid)
  {
    $db = new DHHbankovniizvodiDB();
    $client = null;

    //$clientId = $db->where('auth_user_id', $userid)->getValue('tblclients', 'client_id');

    if (!empty($userid) && $userid != null) {
      $client = $db->where('id', $userid)->getOne("partneri");
    }

    return $client;
  }
}

if (!function_exists('spremiTransakciju')) {
  function spremiTransakciju($transakcijaID, $ponudaID)
  {
    $results = '';
    $db = new DHHbankovniizvodiDB();

    $ponuda = $db->where('id', $ponudaID)->getOne("tblinvoices");
    $iznos = $db->where('oznaka_transakcije', $transakcijaID)->getValue('mod_dhhbankovniizvodi_transakcije', 'iznos');
    if ($ponuda['status'] == 'Unpaid') {
      $command = 'AddTransaction';
      $postData = array(
        'paymentmethod' => 'mailin',
        'invoiceid' => $ponudaID,
        'transid' => $transakcijaID,
        'amountin' => $iznos,
        'date' => date('d/m/Y'),
        'description' => 'Invoice Payment',
        'currencyid' => '1',
      );

      if ($ponuda['total'] == $iznos) {
        $db->where('id', $ponudaID)->update('tblinvoices', array('status' => 'Paid'));
      }

      //$results = localAPI($command, $postData, 'developer');
    }

    if ($results['result'] == "success") {
      return returnMsg(true, 'Transakcija uspješno dodana');
    }
    return returnMsg(false, 'Transakcija nije spremljena');
  }
}
