<?php
// Modul za bankovne izvode

require_once 'functions.php';
?>
<!DOCTYPE html>
<html>

<head>
    <title>Test aplikacija</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
    <link rel="stylesheet" href="inc/font-awesome/css/font-awesome.min.css">
    <link rel="stylesheet" type="text/css" media="all" href="inc/style.css">
    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
    <link rel="stylesheet" href="/resources/demos/style.css">
    <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
    <script>
        $(function() {
            $("#transakcija_input").datepicker({
                dateFormat: 'dd/mm/yy'
            });
        });
    </script>
</head>

<body>
    <?php

    function dhh_bankovni_izvodi_config()
    {
        $configarray = array(
            "name"     => "DHH Bankovni izvodi",
            "version"  => "1.0",
            "author"   => "Ante Mišković",
            "language" => "hrvatski",
            "description" => "Custom modul za import bankovnih izvoda"
        );
        return $configarray;
    }



    $db = new DHHbankovniizvodiDB();
    $transakcija_datum = '';
    $postavkePoruka = '';

    // PROCESIRANJE FORMI
    # postavke
    if (!empty($_POST['aktivnost'])) {
        if ($_POST['aktivnost'] == 'postavke') {
            $db->where('opcija', 'izdavatelj_izvoda')->update('mod_dhhbankovniizvodi', array('vrijednost' => $_POST['izdavatelj_izvoda']));
            $db->where('opcija', 'lokacija_izvoda')->update('mod_dhhbankovniizvodi', array('vrijednost' => $_POST['lokacija_izvoda']));
            $postavkePoruka = '<p class="success">Postavke spremljene</p>';
        }

        # izvodi
        if ($_POST['aktivnost'] == 'izvodi') {
            if (!empty($_FILES["upload_izvoda"])) {
                $postavkePoruka = readReturnMsg(fileUpload($_FILES["upload_izvoda"]));
            }
        }

        $transakcija_datum = '';
        # transakcije
        if ($_POST['aktivnost'] == 'transakcije') {
            if ($_POST['transakcija_datum'] && !empty($_POST['transakcija_datum'])) {
                $transakcija_datum = $_POST['transakcija_datum'];
            } else {
                $postavkePoruka = '<p class="error">Datum nije odabran</p>';
            }
        }
        if ($_POST['aktivnost'] == 'postavke') {
            $db->where('opcija', 'izdavatelj_izvoda')->update('mod_dhhbankovniizvodi', array('vrijednost' => $_POST['izdavatelj_izvoda']));
            $db->where('opcija', 'lokacija_izvoda')->update('mod_dhhbankovniizvodi', array('vrijednost' => $_POST['lokacija_izvoda']));
            $postavkePoruka = '<p class="success">Postavke spremljene</p>';
        }

        # izvodi
        if ($_POST['aktivnost'] == 'izvodi') {
            if (!empty($_FILES["upload_izvoda"])) {
                $postavkePoruka = readReturnMsg(fileUpload($_FILES["upload_izvoda"]));
            }
        }

        # transakcije
        if ($_POST['aktivnost'] == 'transakcije') {
            if ($_POST['transakcija_datum'] && !empty($_POST['transakcija_datum'])) {
                $transakcija_datum = $_POST['transakcija_datum'];
            } else {
                $postavkePoruka = '<p class="error">Datum nije odabran</p>';
            }
        }
    }

    if (isset($_GET['datum'])) {
        $transakcija_datum = $_GET['datum'];
    }

    if (isset($_GET['transakcija']) && isset($_GET['ponuda'])) {
        $transakcijaID = $_GET['transakcija'];
        $ponudaID = $_GET['ponuda'];
        if (!empty($transakcijaID) && !empty($ponudaID)) {
            $postavkePoruka = readReturnMsg(spremiTransakciju($transakcijaID, $ponudaID));
        }
    }

    $stranica = '';
    # provjera aktualne stranice
    if (!empty($_GET['stranica'])) {
        $stranica = $_GET['stranica'];
    }


    // aktivacija/deaktivacija automatskih opcija modula (hookovi)
    if (!empty($_REQUEST['akcija'])) {
        if ($_REQUEST['akcija'] == 'deaktiviraj') {
            $db->where('opcija', 'aktivan')->update('mod_dhhbankovniizvodi', array('vrijednost' => '0'));
            $postavkePoruka = '<p class="success">Funkcije modula uspješno su deaktivirane</p>';
        }
        if ($_REQUEST['akcija'] == 'aktiviraj') {
            $db->where('opcija', 'aktivan')->update('mod_dhhbankovniizvodi', array('vrijednost' => '1'));
            $postavkePoruka = '<p class="success">Funkcije modula uspješno su aktivirane</p>';
        }
    }


    $opcije = opcijedhhbiModula();

    //# add logo
    $html = '<div id="logo"><img src="' . $opcije['sistemrootURL'] . '/dhh_bankovni_izvodi/inc/dhh-logo-100px.png"></a></div>';


    # ================================  OPCIJE - DEFAULT ================================

    if ($stranica == "" || $stranica == "opcije") {
        $html .= '
<div id="cinavbar">
    <ul class="cinav">
    <li class="postavke active"><a href="' . $opcije['modulelink'] . '"><i class="fa fa-gear"></i> POSTAVKE</a></li>
    <li class="izvodi"><a href="' . $opcije['modulelink'] . '&stranica=izvodi"><i class="fa fa-file-code-o"></i> IZVODI</a></li>
    <li class="transakcije"><a href="' . $opcije['modulelink'] . '&stranica=transakcije"><i class="fa fa-exchange"></i> TRANSAKCIJE</a></li>
    </ul>
</div>
<form method="post" id="postavke" name="postavke" class="dhbs_forms" action="' . $opcije['modulelink'] . '">
    <input type="hidden" name="aktivnost" value="postavke" />
    <h1>POSTAVKE MODULA</h1>';

        if ($postavkePoruka)
            echo '<div class="modresult">' . $postavkePoruka . '</div>';

        $html .= '
    <p>
        <label>Izdavatelj bankovnog izvoda:</label>
        <select name="izdavatelj_izvoda" id="izdavatelj_izvoda">
            <option value="---"' . ($opcije['izdavatelj_izvoda'] == '---' ? ' selected' : '') . '>---</option>
            <option value="RBA"' . ($opcije['izdavatelj_izvoda'] == 'RBA' ? ' selected' : '') . '>RBA</option>
            <option value="ZABA"' . ($opcije['izdavatelj_izvoda'] == 'ZABA' ? ' selected' : '') . '>ZABA</option>
        </select>
    </p>
    <p>
        <label for="lokacija_izvoda_input">Lokacija za upload izvoda:</label>
        <input type="text" id="lokacija_izvoda_input" name="lokacija_izvoda" value="' . $opcije['lokacija_izvoda'] . '">
    </p>

    <br>
    <p>
        <label></label>
        <button type="submit" class="zeleni gumb"><i class="fa fa-save"></i> Spremi postavke</button>
    </p> 

</form><br><br>';

        echo $html;
    } // osnovna stranica - opcije


    # ================================  IZVODI  ================================

    if ($stranica == "izvodi") {
        $html .= '
<div id="cinavbar">
<ul class="cinav">
<li class="postavke"><a href="' . $opcije['modulelink'] . '"><i class="fa fa-gear"></i> POSTAVKE</a></li>
<li class="izvodi active"><a href="' . $opcije['modulelink'] . '&stranica=izvodi"><i class="fa fa-file-code-o"></i> IZVODI</a></li>
<li class="transakcije"><a href="' . $opcije['modulelink'] . '&stranica=transakcije"><i class="fa fa-exchange"></i> TRANSAKCIJE</a></li>
</ul>
</div>
<section class="dhbs_forms">
<h1>IZVODI</h1>
';

        if ($postavkePoruka)
            echo '<div class="modresult">' . $postavkePoruka . '</div>';

        $html .= '
<div class="half">
    <form method="post" id="izvodi" name="izvodi" class="dhbs_forms" action="' . $opcije['modulelink'] . '&stranica=' . $stranica . '" enctype="multipart/form-data">
    <input type="hidden" name="aktivnost" value="izvodi" />
    <h2>Upload novog izvoda</h2>
        <p>
            <label for="upload_izvoda">Datoteka za slanje:</label>
            <input type="file" id="upload_izvoda" name="upload_izvoda">
        </p>

        <br>
        <p>
            <label></label>
            <button type="submit" class="zeleni gumb"><i class="fa fa-upload"></i> Pošalji izvod</button>
        </p>
    </form>
</div>

<div class="half">
    <form id="izvodi" name="izvodi" class="dhbs_forms">
    <h2>Datoteke izvoda na serveru</h2>
    <p>';
        $file_list = scanUploadDir();
        foreach ($file_list as $file) {
            $html .= $file . '<br/>';
        }
        $html .= '</p>
    </form>
</div>
<div class="clear"></div>
</section>
';

        echo $html;
    } // izvodi


    # ================================  TRANSAKCIJE ================================

    if ($stranica == "transakcije") {
        $html .= '
<div id="cinavbar">
    <ul class="cinav">
    <li class="postavke"><a href="' . $opcije['modulelink'] . '"><i class="fa fa-gear"></i> POSTAVKE</a></li>
    <li class="izvodi"><a href="' . $opcije['modulelink'] . '&stranica=izvodi"><i class="fa fa-file-code-o"></i> IZVODI</a></li>
    <li class="transakcije active"><a href="' . $opcije['modulelink'] . '&stranica=transakcije"><i class="fa fa-exchange"></i> TRANSAKCIJE</a></li>
    </ul>
</div>
<form method="post" id="postavke" name="postavke" class="dhbs_forms" action="' . $opcije['modulelink'] . '&stranica=' . $stranica . '">
    <input type="hidden" name="aktivnost" value="transakcije" />
    <h1>POPIS TRANSAKCIJA</h1>';

        if ($postavkePoruka)
            echo '<div class="modresult">' . $postavkePoruka . '</div>';

        $html .= '
    <p>
        <label class="mini" for="transakcija_input">Datum transakcija: </label>
        <input type="text" id="transakcija_input" name="transakcija_datum" class="datepick" value="' . $transakcija_datum . '">
    </p>
    <br/>
    <p>
        <label></label>
        <button type="submit" class="zeleni gumb"><i class="fa fa-search"></i> Prikaži transakcije</button>
    </p>

</form>';

        if ($transakcija_datum != null) {
            $html .= '
    <div class="content_container">
    <table class="table" style="font-size: 12px;">
    <thead>
        <tr>
        <th scope="col">Oznaka transakcije</th>
        <th scope="col">Platitelj</th>
        <th scope="col">Poziv na broj</th>
        <th scope="col" style="text-align: right;">Iznos</th>
        <th scope="col">Opis plaćanja</th>
        <th scope="col">Akcije</th>
        </tr>
    </thead>
    <tbody>
';
            $transakcije = dohvatiTransakcijeZaDatum($transakcija_datum);
            if (!empty($transakcije)) {
                foreach ($transakcije as $transakcija) {
                    $klijent = null;
                    $ponuda = null;
                    $noData = "----------";

                    if (!empty($transakcija['poziv_na_broj']) && $transakcija['poziv_na_broj'] != null) {
                        $ponuda = dohvatiPonudu($transakcija['poziv_na_broj']);
                    }
                    //$korisnik = dohvatiKorisnika($ponuda['userid']);

                    if (!empty($ponuda) && $ponuda != null) {
                        $klijent = dohvatiKlijenta($ponuda['userid']);
                    }

                    $oznaka_transakcije = $transakcija['oznaka_transakcije'];
                    $oznaka_transakcije_provjera = $ponuda['status'];
                    $platitelj = $transakcija['platitelj'];
                    $platitelj_provjera = ($klijent != null && !empty($klijent)) ? $klijent['naziv'] : $noData;
                    $poziv_na_broj = $transakcija['poziv_na_broj'];
                    $poziv_na_broj_provjera = ($ponuda['invoicenum'] == $poziv_na_broj && !empty($poziv_na_broj)) ? $ponuda['invoicenum'] : $noData;
                    $iznos = $transakcija['iznos'] . ' ' . $transakcija['valuta'];
                    $iznos_provjera = ($ponuda['total'] != null && !empty($ponuda['total'])) ? $ponuda['total'] . ' ' . $transakcija['valuta'] : $noData;

                    $poziv_na_broj_klasa = ($poziv_na_broj == $poziv_na_broj_provjera && !empty($poziv_na_broj)) ? "table-success" : "table-danger";
                    $iznos_klasa = ($iznos == $iznos_provjera && !empty($iznos)) ? "table-success" : "table-danger";
                    $platitelj_klasa = ($platitelj == $platitelj_provjera && !empty($platitelj)) ? "table-success" : "table-danger";

                    $ponuda_link = ($ponuda['id'] != null && !empty($ponuda['id']))  ? '<a href="invoices.php?action=edit&id=' . $ponuda['id'] . '" target="_blank"><i class="fa fa-pencil-square-o"></i></a>' : "";
                    $dodaj_transakciju_link = ($oznaka_transakcije != null && $oznaka_transakcije != $noData && !empty($oznaka_transakcije) && $poziv_na_broj == $poziv_na_broj_provjera)  ? '<a href="' . $opcije['modulelink'] . '&stranica=' . $stranica . '&datum=' . $transakcija_datum . '&transakcija=' . $oznaka_transakcije . '&ponuda=' . $ponuda['id'] . '"><i class="fa fa-check-square-o"></i></a>' : "";
                    $placeno_klasa = ($oznaka_transakcije_provjera == 'Paid') ? "table-success" : "";

                    if ($oznaka_transakcije_provjera == 'Paid') {
                        $ponuda_link = '';
                        $dodaj_transakciju_link = '';
                    }

                    $html .= '
        <tr>
        <td class="' . $placeno_klasa . '">' . $oznaka_transakcije . '<br/>' . $oznaka_transakcije_provjera . '</td>
        <td class="' . $platitelj_klasa . '">' . $platitelj . '<br/>' . $platitelj_provjera . '</td>
        <td class="' . $poziv_na_broj_klasa . '">' . $poziv_na_broj . '<br/>' . $poziv_na_broj_provjera . '</td>
        <td class="' . $iznos_klasa . '"style="text-align: right;">' . $iznos . '<br/>' . $iznos_provjera . '</td>
        <td width="400px;">' . $transakcija['opis_placanja'] . '</td>
        <td class="icons" width="80px;">' . $ponuda_link . '' . $dodaj_transakciju_link . '</td>
        </tr>';
                }
            }
            $html .= '
    </tbody>
    </table>
    </div>
    ';
        }

        echo $html;
    } // osnovna stranica - opcije

    ?>
</body>

</html>